# CubeSat Agile - Automatic Scheduler

[x] Basic task scheduling
[x] Basic task fetch from queue
[x] Delayed tasks
[x] Run tasks
[x] task status update
[x] Task retry
[x] Goal parsing
[x] Goal update
[x] Scheduler task scheduling
[x] handle immediate_next properly
[x] Try RedisJSON - 9th Aug
[x] Refactor task structure and revisit all schduler functions 10th Aug
[x] Robust check, go through all unwrap 11th Aug
[x] Locking
[x] INFO and WARNING logging
[x] Remove unecessary Error Logging
[x] FIX work_task()
[x] Redis restart by program
[ ] Proper testing suite + unit testing
[ ] dockerise
[ ] Need more user story input


## Current pattern on OBC

1. Ground station publishes to redis channel "channel_goal" the entire goal as json string.
2. Subscriber to the channel (A thread) will poll this string -> store the goal as a(goal:goal_id) hash -> store its tasks as (task: task_id) hash  -> keep a list of tasks as (tasks:goal_id) list.
3. Schedule the first task from (tasks:goal_id).
4. Worker will get this task, update task status to (task:task_id) and depending if task.immediate_next or not it will keep running that task lists, if not it will schedule next task. 
5. When the list is fully popped, scheduler check if Goal is repeated or not, if so, rebuild the task lists in step 2.
6. Otherwise mark as complete.



## How to run
TODO: dockerise all this
### Install redis
1. `cd redis-6.2.6`
2. run `make` to install redis
3. Check installation with `make test`

### Install redisJSON
1. librejson.so is already provided in directory, however in different hardware it might need to be recompiled. Try running `./redis-6.2.6/src/redis-server --loadmodule ../../librejson.so` in root first.
2. get redisJSON from `git clone https://github.com/RedisJSON/RedisJSON.git`
3. run `cargo build --release` to compile
4. run `cargo test --features test` to test 
5. get file from `./target/release/librejson.so`, replace the one under root

### Linking and running
1. run redis-server in redis-6.2.6/src
2. run `./redis-6.2.6/src/redis-server --loadmodule ./librejson.so --daemonize yes` from this folder to run redis in background
3. To shutdown, `./redis-6.2.6/src/redis-cli` to run command line interface and run command `shutdown`

### Link kubos before compiling
1. `vim Cargo.toml`
2. change kubos-app path to: `"/home/ben/kubos/apis/app-api/rust"`

## About locking

No two threads should be working on the same goal, hence lock name is the same as goal_id. This is subject to change therefore an option to have a different lock_name is given. Potentially this step could be integrated with getter and setter (i.e. one get must go with one set), however I'm leaving it for flexibility.