use crate::log;
use crate::log::Log;
use chrono::Utc;
use redis;
use redis::Commands;
use serde::{Deserialize, Serialize};
use std::cmp::{max, min};
use std::process::Command;
use uuid;

#[derive(Deserialize, Serialize)]
pub struct Task {
    pub goal_id: String,
    pub index: usize,
    pub name: String,
    pub args: Vec<String>,
    pub priority: Priority,
    pub next: Vec<usize>,
    pub skip_to: usize,
    pub delay: i64,
    pub retries: i64,
    pub status: TaskStatus,
    pub abort_type: AbortType,
    pub immediate_next: bool,
}
#[derive(Deserialize, Serialize, PartialEq, Clone)]
pub enum Priority {
    URGENT,
    HIGH,
    NORMAL,
}
#[derive(Deserialize, Serialize, PartialEq, Clone)]
pub enum TaskStatus {
    Completed,
    Error,
    Skipped,
    Working,
    Queued,
    Incomplete,
}
#[derive(Deserialize, Serialize, Clone)]
pub enum AbortType {
    Panic,
    Skip,
}

#[derive(Deserialize, Serialize, Clone, PartialEq)]
pub enum GoalStatus {
    Scheduled,
    Completed,
    Error,
    Idle,
}

#[derive(Deserialize, Serialize)]
pub struct Goal {
    pub id: String,
    pub name: String,
    pub tasks: Vec<Task>,
    pub n_tasks: usize,
    pub status: GoalStatus,
    pub time: i64,
    pub repeat: i64,
    pub n_repeated: i64,
    pub repeat_interval: i64,
    pub logs: Vec<Log>,
}
const UNLOCK_SCRIPT: &str = r"if redis.call('get',KEYS[1]) == ARGV[1] then
                            return redis.call('del',KEYS[1])
                            else
                            return 0
                            end";

pub fn connect_redis(addr: &str) -> Result<redis::Connection, ()> {
    let attempt = redis::Client::open(addr);
    let client;
    match attempt {
        Ok(c) => client = c,
        Err(_) => {
            Command::new("./redis-6.2.6/src/redis-server")
                .args(&["--loadmodule", "./librejson.so", "--daemonize", "yes"])
                .spawn()
                .expect("Failed to connect and start redis, abort system");
            println!("{}", addr);
            client = redis::Client::open(addr).unwrap();
        }
    }
    let mut retries = 0;
    while retries < 2 {
        match client.get_connection() {
            Ok(con) => return Ok(con),
            Err(_) => {
                Command::new("./redis-6.2.6/src/redis-server")
                    .args(&["--loadmodule", "./librejson.so", "--daemonize", "yes"])
                    .spawn()
                    .expect("Failed to connect and start redis, abort system");
                retries += 1;
            }
        }
    }
    return Err(());
}
/*
    Get goal from database
    1. Checks if id exists in the form of key 'goal-{id}'
    2. Check if the value is json and parses to goal
    3. return goal if there is one
*/
pub fn get_goal(id: &str, con: &mut redis::Connection) -> Option<Goal> {
    let value_type: String = redis::cmd("TYPE")
        .arg(format!("goal:{}", id))
        .query(con)
        .unwrap();
    if value_type != "ReJSON-RL" {
        return None;
    } else {
        let goal_str: String = redis::cmd("JSON.GET")
            .arg(format!("goal:{}", id))
            .arg(".")
            .query(con)
            .unwrap();
        match serde_json::from_str(&goal_str) {
            Ok(goal) => goal,
            Err(_) => None,
        }
    }
}

/*
    Set goal. No checking
*/
pub fn set_goal(goal: &Goal, con: &mut redis::Connection) -> Result<(), ()> {
    match serde_json::to_string(goal) {
        Ok(goal_str) => {
            let query: std::result::Result<bool, redis::RedisError> = redis::cmd("JSON.SET")
                .arg(format!("goal:{}", goal.id))
                .arg(".")
                .arg(goal_str)
                .query(con);
            if let Err(err) = query {
                log::error(
                    &goal.id,
                    &format!("Failed to set goal, error message: {}", err),
                    con,
                );
                return Err(());
            } else {
                return Ok(());
            }
        }
        Err(err) => {
            log::error(
                &goal.id,
                &format!("Goal object parse failed, error message:{}", err),
                con,
            );
            return Err(());
        }
    }
}

/*
    get a certain task
*/
pub fn get_task(goal_id: &str, task_index: usize, con: &mut redis::Connection) -> Result<Task, ()> {
    let query = get_goal(&goal_id, con);
    match query {
        Some(goal) => {
            if goal.n_tasks < task_index {
                log::error(
                    goal_id,
                    &format!(
                        "Getting task failed, index out of range, length:{} index:{}",
                        goal.n_tasks, task_index
                    ),
                    con,
                );
                Err(())
            } else {
                let task_str: String = redis::cmd("JSON.GET")
                    .arg(format!("goal:{}", goal_id))
                    .arg(format!(".tasks[{}]", task_index))
                    .query(con)
                    .unwrap();
                if let Ok(res) = serde_json::from_str(&task_str) {
                    Ok(res)
                } else {
                    log::error(goal_id, "Task corrupted", con);
                    Err(())
                }
            }
        }
        None => {
            log::error(
                goal_id,
                &format!("Could not get goal, could be corrupted"),
                con,
            );
            Err(())
        }
    }
}

pub fn set_task(
    goal_id: &str,
    task_index: usize,
    task: &Task,
    con: &mut redis::Connection,
) -> Result<(), ()> {
    if let Some(goal) = get_goal(goal_id, con) {
        if goal.n_tasks < task_index {
            log::error(
                goal_id,
                &format!(
                    "Getting task failed, index out of range, length:{} index:{}",
                    goal.n_tasks, task_index
                ),
                con,
            );
            Err(())
        } else {
            let _: String = redis::cmd("JSON.SET")
                .arg(format!("goal:{}", goal_id))
                .arg(format!(".tasks[{}]", task_index))
                .arg(serde_json::to_string(task).unwrap())
                .query(con)
                .unwrap();
            Ok(())
        }
    } else {
        Err(())
    }
}

pub fn schedule_task(
    goal_id: &str,
    task_index: usize,
    goal_delay: i64,
    con: &mut redis::Connection,
) -> Result<(), ()> {
    let mut task: Task;
    if let Ok(data) = get_task(goal_id, task_index, con) {
        task = data;
    } else {
        log::error(
            goal_id,
            &format!("Could not get task in index {}", task_index),
            con,
        );
        return Err(());
    }

    match &task.status {
        TaskStatus::Working => {
            log::error(
                goal_id,
                &format!(
                    "Task {} of index {} is currently working",
                    task.name, task.index
                ),
                con,
            );
            return Err(());
        }
        TaskStatus::Queued => {
            log::error(
                goal_id,
                &format!(
                    "Task {} of index {} is already queued",
                    task.name, task.index
                ),
                con,
            );
            return Err(());
        }
        _ => {}
    }
    task.status = TaskStatus::Queued;
    let queue;
    match &task.priority {
        Priority::HIGH => queue = "high",
        Priority::NORMAL => queue = "normal",
        Priority::URGENT => queue = "urgent",
    }
    let query: std::result::Result<i64, redis::RedisError>;
    if task.delay > 0 || goal_delay > 0 {
        query = redis::cmd("ZADD")
            .arg("queue:delayed")
            .arg(max(task.delay, 0) + Utc::now().timestamp() + max(goal_delay, 0))
            .arg(serde_json::to_string(&task).unwrap())
            .query(con);
    } else {
        query = redis::cmd("RPUSH")
            .arg(format!("queue:{}", queue))
            .arg(serde_json::to_string(&task).unwrap())
            .query(con);
    }
    if let Err(err) = query {
        log::error(
            goal_id,
            &format!(
                "Encountered a redis error while scheduling task {} of index {}, message: {}",
                task.name, task.index, err
            ),
            con,
        );
        return Err(());
    } else {
        if let Err(_) = set_task(goal_id, task_index, &task, con) {
            log::error(
                goal_id,
                &format!("Could not set task {} of index {}", task.name, task.index),
                con,
            );
            return Err(());
        }
        return Ok(());
    }
}

pub fn goal_exists(id: &str, con: &mut redis::Connection) -> bool {
    let query = redis::cmd("EXISTS").arg(format!("goal:{}", id)).query(con);
    if let Ok(ret) = query {
        return ret;
    } else {
        return false;
    }
}

pub fn acquire_lock(
    lock_name: &str,
    goal_id: &str,
    acquire_timeout: i64,
    lock_timeout: usize,
    con: &mut redis::Connection,
) -> Result<String, ()> {
    let identifier: String = uuid::Uuid::new_v4().to_string();

    let end = Utc::now().timestamp() + acquire_timeout;

    while Utc::now().timestamp() < end {
        let query: std::result::Result<bool, redis::RedisError> = redis::cmd("SETNX")
            .arg(format!("lock:{}", lock_name))
            .arg(&identifier)
            .query(con);
        if let Ok(success) = query {
            if success {
                let time_out: std::result::Result<i64, redis::RedisError> =
                    con.expire(format!("lock:{}", lock_name), lock_timeout);
                if let Err(_) = time_out {
                    log::error(
                        goal_id,
                        &format!("failed to set timeout on lock name:{}, fatal", lock_name),
                        con,
                    );
                    return Err(());
                }
                return Ok(identifier);
            }
            let has_timeout: std::result::Result<i64, redis::RedisError> =
                con.ttl(format!("lock:{}", lock_name));
            match has_timeout {
                Ok(timeout) => {
                    if timeout == -1 {
                        let time_out: std::result::Result<i64, redis::RedisError> =
                            con.expire(format!("lock:{}", lock_name), lock_timeout);
                        if let Err(_) = time_out {
                            log::error(
                                goal_id,
                                &format!("failed to set timeout on lock name:{}, fatal", lock_name),
                                con,
                            );
                            return Err(());
                        }
                    } else if timeout == -2 {
                        log::error(
                            goal_id,
                            &format!(
                                "Lock:{} does not exist while trying to check TTL",
                                lock_name
                            ),
                            con,
                        );
                        return Err(());
                    }
                }
                Err(_) => {
                    log::error(
                        goal_id,
                        &format!("Lock:{} failed to check TTL", lock_name),
                        con,
                    );
                    return Err(());
                }
            }
            log::error(
                goal_id,
                &format!(
                    "Database object locked name:{}, retrying after 1 second",
                    lock_name
                ),
                con,
            );
            std::thread::sleep(std::time::Duration::from_millis(1000));
        } else {
            log::error(
                goal_id,
                "Redis error on SETNX lock, please check set up",
                con,
            );
            return Err(());
        }
    }
    log::error(
        goal_id,
        &format!(
            "Acquiring lock name:{} timed out, potential collision",
            lock_name
        ),
        con,
    );
    return Err(());
}

pub fn release_lock(
    lockname: &str,
    goal_id: &str,
    identifier: &str,
    con: &mut redis::Connection,
) -> Result<(), ()> {
    let lock_name = &format!("lock:{}", lockname);
    // let pipe = redis::pipe();
    let script = redis::Script::new(UNLOCK_SCRIPT);
    let result: redis::RedisResult<i32> = script.key(&[lock_name]).arg(&[identifier]).invoke(con);

    match result {
        Ok(_) => return Ok(()),
        Err(_) => {
            log::error(
                goal_id,
                &format!("Could not release lock for goal:{}", goal_id),
                con,
            );
            return Err(());
        }
    }
}
