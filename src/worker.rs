use crate::database;
use crate::database::AbortType;
use crate::database::Task;
use crate::database::TaskStatus;
use crate::log;
use crate::redis_helper;
use crate::scheduler;
use crate::tasks::TaskMap;
use chrono::Utc;
use serde_json;
use std::time;
pub fn start() {
    none_delay_queue();
    delay_queue();
}
fn none_delay_queue() -> std::thread::JoinHandle<String> {
    let mut conn = database::connect_redis("redis://127.0.0.1/").unwrap();
    let handle = std::thread::spawn(move || loop {
        let query: redis::RedisResult<Vec<String>> = redis::cmd("BLPOP")
            .arg("queue:urgent")
            .arg("queue:high")
            .arg("queue:normal")
            .arg(0)
            .query(&mut conn);
        let task_str;
        if let Ok(task_tuple) = query {
            task_str = task_tuple[1].clone();
        } else {
            log::system("non delay queue pop fail, repeating.");
            continue;
        }
        // println!("Received task from queue: {}", tuple.1);
        if let Err(_) = work_task(&task_str, &mut conn) {
            log::system(&format!("Error working task: {}", &task_str));
            if let Err(_) = fail_task(&task_str, &mut conn) {
                log::system(&format!(
                    "failed to gracefully fail a task, str {}",
                    &task_str
                ));
            }
        }
        // schedule next_id
    });
    handle
}

fn delay_queue() -> std::thread::JoinHandle<String> {
    let mut conn = redis_helper::Conn::new("redis://127.0.0.1/");
    let handle = std::thread::spawn(move || loop {
        let item;
        let time: i64;
        if let Some(v) = conn.zcheck("queue:delayed") {
            item = v.0;
            time = v.1.parse().unwrap();
            if time > Utc::now().timestamp() {
                std::thread::sleep(time::Duration::from_millis(1000));
                continue;
            };
        } else {
            std::thread::sleep(time::Duration::from_millis(1000));
            continue;
        }

        if conn.zrem("queue:delayed", &item) > 0 {
            conn.rpush("queue:normal", &item);
        }
    });
    handle
}

fn work_task(task_str: &String, con: &mut redis::Connection) -> Result<(), ()> {
    // compare task string with task in goal
    // if they are different, do not work task (?),
    // work task otherwise

    let mut task: Task;
    if let Ok(t) = serde_json::from_str::<Task>(task_str) {
        task = t;
    } else {
        log::system(&format!("Could not recognise task: {}", &task_str));
        return Err(());
    }

    let lock = database::acquire_lock(&task.goal_id, &task.goal_id, 3, 10, con)?;
    let db_task: Task;
    if let Ok(t) = database::get_task(&task.goal_id, task.index, con) {
        db_task = t;
    } else {
        log::system(&format!("Could not get task: {}", &task_str));
        return Err(());
    }

    let db_task_str = serde_json::to_string(&db_task).unwrap();
    if db_task_str.ne(task_str) {
        log::error(
            &db_task.goal_id,
            &format!(
                "Task in queue did not match task in database. Index: {}\n In db:\n{} \n In argument:\n{}\n",
                db_task.index, &db_task_str, &task_str,
            ),
            con,
        );
        return Err(());
    }

    log::info(&task.goal_id, &format!("Working task {}", &task.name), con);

    // should not work if working
    if task.status == TaskStatus::Working {
        log::error(
            &db_task.goal_id,
            &format!("Task was already running. Index: {}", db_task.index),
            con,
        );
        return Err(());
    }
    // mark as working
    task.status = TaskStatus::Working;
    if let Err(_) = database::set_task(&task.goal_id, task.index, &task, con) {
        log::error(
            &task.goal_id,
            &format!(
                "Failed to set task status before working the task at index:{}",
                &task.index
            ),
            con,
        );
        return Err(());
    }
    // call the task
    // TODO: store map at start up
    let task_map = TaskMap::new().map;
    let ret: Result<usize, ()> = loop {
        match task_map.get(&task.name) {
            Some(callback) => {
                if let Ok(i) = callback(&task.args, &task.goal_id) {
                    break Ok(i);
                } else {
                    if task.retries > 0 {
                        task.retries -= 1;
                        log::warning(
                            &task.goal_id,
                            &format!(
                                "Task {} failed, retrying... {} retries remaining",
                                &task.name, &task.retries
                            ),
                            con,
                        );
                        continue;
                    } else {
                        task.status = TaskStatus::Error;
                        if let Err(_) = database::set_task(&task.goal_id, task.index, &task, con) {
                            log::error(
                                &task.goal_id,
                                &format!(
                                    "Failed to set task status after working the task at index:{}",
                                    &task.index
                                ),
                                con,
                            );
                            database::release_lock(&task.goal_id, &task.goal_id, &lock, con)?;
                            return Err(());
                        }
                        database::release_lock(&task.goal_id, &task.goal_id, &lock, con)?;
                        break Err(());
                    }
                }
            }
            None => {
                log::error(
                    &task.goal_id,
                    &format!("Could not find task named {}", &task.name),
                    con,
                );
                database::release_lock(&task.goal_id, &task.goal_id, &lock, con)?;
                return Err(());
            }
        }
    };

    if let Ok(next_index) = ret {
        // mark task as complete
        task.status = TaskStatus::Completed;
        if let Err(_) = database::set_task(&task.goal_id, task.index, &task, con) {
            log::error(
                &task.goal_id,
                &format!(
                    "Failed to set task status after working the task at index:{}",
                    &task.index
                ),
                con,
            );
            database::release_lock(&task.goal_id, &task.goal_id, &lock, con)?;
            return Err(());
        }
        database::release_lock(&task.goal_id, &task.goal_id, &lock, con)?;
        // find next task
        // check next index is within task.next
        if next_index >= task.next.len() {
            log::error(
                &task.goal_id,
                &format!(
                    "task at index {} was not set up to handle the output of the payload function; returned {}",
                    &task.index, next_index
                ),
                con,
            );
            return Err(());
        }
        log::info(
            &task.goal_id,
            &format!("Completed task: {}", &task.name),
            con,
        );
        let next_task_number = task.next[next_index];
        if task.immediate_next {
            if let Ok(next_task) = database::get_task(&task.goal_id, next_task_number, con) {
                let next_task_str = serde_json::to_string(&next_task).unwrap();
                return work_task(&next_task_str, con);
            } else {
                log::error(
                    &task.goal_id,
                    &format!("Could not get next task of task index {}", &task.index),
                    con,
                );
                return Err(());
            }
        } else {
            scheduler::schedule_task_index(&task.goal_id, next_task_number, 0, con)
        }
    } else {
        return Err(());
    }
}

fn fail_task(task_str: &str, con: &mut redis::Connection) -> Result<(), ()> {
    let goal_id;
    let index;
    if let Ok(t) = serde_json::from_str::<Task>(task_str) {
        goal_id = t.goal_id.clone();
        index = t.index;
    } else {
        return Err(());
    }

    let lock = database::acquire_lock(&goal_id, &goal_id, 10, 10, con)?;
    let task = database::get_task(&goal_id, index, con)?;
    database::release_lock(&goal_id, &goal_id, &lock, con)?;
    match task.abort_type {
        // skip to
        AbortType::Skip => {
            return scheduler::skip_tasks(&task.goal_id, task.index, task.skip_to, con);
        }
        _ => {
            // set goal to error
            return Ok(scheduler::abort_goal(&task.goal_id, con));
        }
    }
}
