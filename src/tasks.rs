use crate::database;
use crate::log;
use failure::Error;
use kubos_app::query;
use kubos_app::ServiceConfig;
use redis::Commands;
use std::collections::HashMap;
use std::time;
type Index = usize;
type Callback = fn(&Vec<String>, &str) -> Result<Index, Error>;

pub struct TaskMap {
    pub map: HashMap<String, Callback>,
}
impl TaskMap {
    pub fn new() -> TaskMap {
        let map = {
            let mut hashmap = HashMap::new();
            hashmap.insert("print".to_string(), print_callback as Callback);
            hashmap.insert("power_on".to_string(), power_on_callback as Callback);
            hashmap.insert("ex_power_on".to_string(), ex_power_on as Callback);
            hashmap.insert("ex_power_off".to_string(), ex_power_off as Callback);
            hashmap.insert("ex_payload".to_string(), ex_payload as Callback);
            hashmap.insert("ex_count".to_string(), ex_count as Callback);
            hashmap.insert("odd_even_time".to_string(), odd_even_timestamp as Callback);
            hashmap.insert("ex_check_battery".to_string(), ex_check_battery as Callback);
            hashmap
        };
        TaskMap { map }
    }
}
fn print_callback(args: &Vec<String>, _goal_id: &str) -> Result<Index, Error> {
    print!("Print value: ");
    for arg in args {
        print!("{} ", arg);
    }
    print!("\n");
    Ok(0)
}

fn test() -> Result<String, ()> {
    Ok("done".to_string())
}
fn power_on_callback(_args: &Vec<String>, _goal_id: &str) -> Result<Index, Error> {
    fn power_on(svc: ServiceConfig) -> Result<serde_json::Value, Error> {
        let query_str = r"
        mutation {
        epsSetSingleOutput(channel:gnssr,powerState:On){
          errors,success
          }
        }";
        const DEFAULT_TIMEOUT: Option<time::Duration> = Some(time::Duration::from_secs(10));
        return query(&svc, query_str, DEFAULT_TIMEOUT);
    }

    match power_on(ServiceConfig::new("FakeEPS-service")?) {
        Ok(_) => (Ok(0)),
        Err(err) => {
            // error!("EPS not responding {}", err);
            Err(err)
        }
    }
}

fn ex_power_on(_args: &Vec<String>, goal_id: &str) -> Result<Index, Error> {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    let power: Option<String> = con.get("ex_power").unwrap();
    if let Some(o) = power {
        if o == "1" {
            return Err(failure::format_err!("Power already on"));
        } else if o == "-1" {
            // -1 if not enough battery
            log::warning(
                goal_id,
                "Could not turn on power, battery level too low",
                &mut con,
            );
            return Ok(1);
        }
    };
    let _: String = con.set("ex_power", "1").unwrap();
    Ok(0)
}

fn ex_power_off(_args: &Vec<String>, _goal_id: &str) -> Result<Index, Error> {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    let power: Option<String> = con.get("ex_power").unwrap();
    if let Some(o) = power {
        if o == "0" {
            return Err(failure::format_err!("Power already off"));
        };
    } else {
        return Err(failure::format_err!("Power already off"));
    }
    let _: String = con.set("ex_power", "0").unwrap();
    Ok(0)
}

fn ex_payload(args: &Vec<String>, goal_id: &str) -> Result<Index, Error> {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    if args.len() < 1 {
        log::error(goal_id, "Not enough arguments", &mut con);
        return Err(failure::format_err!("Not enough arguments"));
    };
    if let Ok(res) = args[0].parse::<Index>() {
        return Ok(res);
    }
    log::error(
        goal_id,
        "Argument is not String Index for payload task",
        &mut con,
    );
    return Err(failure::format_err!("Arguments are not type Index"));
}

/// internal counter associated with goal_id
/// args: num<usize> to count to
fn ex_count(args: &Vec<String>, goal_id: &str) -> Result<Index, Error> {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    if args.len() < 1 {
        log::error(goal_id, "Not enough arguments", &mut con);
        return Err(failure::format_err!("Not enough arguments"));
    };

    let upto;
    match args[0].parse::<Index>() {
        Ok(n) => upto = n,
        Err(_) => return Err(failure::format_err!("Argument not positive integer")),
    }
    match con.incr::<String, i64, Index>(format!("count:{}", goal_id), 1) {
        Ok(x) => {
            log::info(goal_id, &format!("Count is {}", x), &mut con);
            if upto == x {
                log::info(goal_id, "Finished counting", &mut con);
                con.del::<String, i64>(format!("count:{}", goal_id))
                    .unwrap();
                return Ok(1);
            }
            return Ok(0);
        }
        Err(_) => return Err(failure::format_err!("Redis failure, key occupied")),
    }
}

fn odd_even_timestamp(_args: &Vec<String>, goal_id: &str) -> Result<Index, Error> {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    if let Some(goal) = database::get_goal(goal_id, &mut con) {
        if goal.time % 5 == 0 {
            return Ok(2);
        } else if goal.time % 2 != 0 {
            return Ok(1);
        } else {
            return Ok(0);
        }
    } else {
        return Err(failure::format_err!("Goal missing."));
    }
}

fn ex_check_battery(_args: &Vec<String>, goal_id: &str) -> Result<Index, Error> {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    let power: Option<String> = con.get("ex_power").unwrap();
    if let Some(o) = power {
        if o == "0" {
            return Err(failure::format_err!("Power is off"));
        };
        log::info(goal_id, &format!("Power level is {}", o), &mut con);
    } else {
        return Err(failure::format_err!("Power is off"));
    }
    Ok(0)
}
