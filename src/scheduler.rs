extern crate redis;
use crate::database;
use crate::database::{AbortType, Goal, GoalStatus, Task, TaskStatus};
use crate::log;
use chrono::Utc;
use redis::Commands;
use serde::{Deserialize, Serialize};
#[derive(Deserialize, Serialize)]
pub struct InputTask {
    pub name: String,
    pub args: Vec<String>,
    pub priority: database::Priority,
    pub next: Vec<usize>,
    pub skip_to: usize,
    pub status: TaskStatus,
    pub delay: i64,
    pub retries: i64,
    pub abort_type: AbortType,
    pub immediate_next: bool,
}
#[derive(Deserialize, Serialize)]
pub struct InputGoal {
    pub id: String,
    pub name: String,
    pub tasks: Vec<InputTask>,
    pub status: GoalStatus,
    pub time: i64,
    pub repeat: i64,
    pub n_repeated: i64,
    pub repeat_interval: i64,
}
/// Takes simplier input goal mapping it to goals with additional information
fn map_input_goal(input: &InputGoal) -> Result<Goal, ()> {
    let mut out = Goal {
        id: input.id.clone(),
        name: input.name.clone(),
        tasks: vec![],
        n_tasks: input.tasks.len(),
        status: input.status.clone(),
        time: input.time,
        repeat: input.repeat,
        n_repeated: input.n_repeated,
        repeat_interval: input.repeat_interval,
        logs: vec![],
    };
    if out.status == GoalStatus::Idle {
        out.status = GoalStatus::Scheduled;
    }
    let mut tasks: Vec<Task> = vec![];
    for i in 0..input.tasks.len() {
        let build = Task {
            goal_id: input.id.clone(),
            index: i,
            name: input.tasks[i].name.clone(),
            args: input.tasks[i].args.clone(),
            priority: input.tasks[i].priority.clone(),
            next: input.tasks[i].next.clone(),
            skip_to: input.tasks[i].skip_to,
            delay: input.tasks[i].delay,
            retries: input.tasks[i].retries,
            status: input.tasks[i].status.clone(),
            abort_type: input.tasks[i].abort_type.clone(),
            immediate_next: input.tasks[i].immediate_next,
        };
        tasks.push(build);
    }
    out.tasks = tasks;
    Ok(out)
}

/// Main channel that listens to InputGoal string
/// Should only run one of this.
pub fn start_goal_channel() -> std::thread::JoinHandle<String> {
    let mut con = database::connect_redis("redis://127.0.0.1/").unwrap();

    return std::thread::spawn(move || {
        let mut pubsub = con.as_pubsub();
        pubsub.subscribe("goal_channel").unwrap();
        loop {
            let msg = pubsub.get_message().unwrap();
            let payload: String = msg.get_payload().unwrap();
            println!("Received Payload on goal_channel: {}", &payload);
            if payload.starts_with("delete") {
                let args: Vec<&str> = payload.split(" ").collect();
                if args.len() < 2 {
                    log::system("no goal id is given while a delete key is received");
                    continue;
                }
                if let Err(_) = delete_goal(args[1]) {
                    log::system(&format!(
                        "Something went wrong while trying to delete goal:{}",
                        args[1]
                    ))
                }
                continue;
            }
            match serde_json::from_str::<InputGoal>(&payload) {
                Ok(input_goal) => {
                    let goal: Goal;
                    match map_input_goal(&input_goal) {
                        Ok(g) => goal = g,
                        Err(_) => {
                            log::system("map_input_goal failed at channel");
                            continue;
                        }
                    }
                    // get connection
                    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
                    let mut con = client.get_connection().unwrap();
                    // Check if it exists
                    let goal_exists = database::goal_exists(&goal.id, &mut con);
                    if let Err(_) = database::set_goal(&goal, &mut con) {
                        log::error(
                            &goal.id,
                            "Error in channel: Goal could not be set",
                            &mut con,
                        );
                    } else {
                        log::info(&goal.id, "Goal saved success", &mut con);
                        if !goal_exists {
                            // schedule first task of brand new goal
                            let delay = goal.time - Utc::now().timestamp();
                            if let Err(_) = schedule_task_index(&goal.id, 0, delay, &mut con) {
                                log::error(
                                    &goal.id,
                                    "Error in channel: Could not schedule first task of goal",
                                    &mut con,
                                );
                                continue;
                            } else {
                                log::system(&format!(
                                    "Goal {} of id {} successfully scheduled",
                                    &goal.name, &goal.id
                                ))
                            }
                        } else {
                            log::info(&goal.id, "Goal updated", &mut con);
                            log::system(&format!(
                                "Goal {} of id {} updated.",
                                &goal.name, &goal.id
                            ));
                        }
                    }
                }
                Err(err) => {
                    log::system("Error parsing a goal.");
                    println!("{}", err);
                    continue;
                }
            }
        }
    });
}

/// Schedule a task in the given goal (by ID) and provided index
pub fn schedule_task_index(
    goal_id: &str,
    index: usize,
    goal_delay: i64,
    con: &mut redis::Connection,
) -> Result<(), ()> {
    log::info(goal_id, &format!("Scheduling task in index {}", index), con);
    match database::get_goal(goal_id, con) {
        Some(goal) => {
            // complete goal if given index is larger than task list or negative
            if goal.tasks.len() - 1 < index {
                return complete_goal(goal_id, con);
            } else {
                return database::schedule_task(goal_id, index, goal_delay, con);
            }
        }
        None => {
            log::error(goal_id, "Could not get goal", con);
            return Err(());
        }
    }
}

pub fn complete_goal(goal_id: &str, con: &mut redis::Connection) -> Result<(), ()> {
    // check if goal need to repeat
    // save snapshot
    log::info(goal_id, "Goal complete, running complete_goal", con);
    let mut goal;
    let lock = database::acquire_lock(goal_id, goal_id, 3, 10, con)?;
    if let Some(g) = database::get_goal(goal_id, con) {
        goal = g;
    } else {
        log::error(
            goal_id,
            &format!("Could not find goal in complete_goal({}, con)", goal_id),
            con,
        );
        return Err(());
    }

    // repeat
    // > 0: repeat for that many times
    // = 0: no repeat
    // < 0: infinite repeat
    if goal.repeat == goal.n_repeated {
        // mark completed if completed, and return
        goal.status = database::GoalStatus::Completed;
        if let Err(_) = database::set_goal(&goal, con) {
            log::error(
                goal_id,
                &format!("Could not save goal in complete_goal({}, con)", goal_id),
                con,
            );
            return Err(());
        }
        log::info(goal_id, "Goal successfully completed", con);
        return database::release_lock(goal_id, goal_id, &lock, con);
    } else {
        // increase n_repeated by 1
        if let Err(_) = database::set_goal(&goal, con) {
            log::error(
                goal_id,
                &format!("Could not save goal in complete_goal({}, con)", goal_id),
                con,
            );
            return Err(());
        }
        database::release_lock(goal_id, goal_id, &lock, con)?;
        // schedule first task
        return schedule_repeat_goal(goal_id, con);
    }
}
pub fn skip_tasks(
    goal_id: &str,
    from: usize,
    to: usize,
    con: &mut redis::Connection,
) -> Result<(), ()> {
    // mark tasks in between as skipped
    log::warning(
        goal_id,
        &format!("Skipping task from {} to {}", from, to),
        con,
    );
    let mut goal;
    let lock = database::acquire_lock(goal_id, goal_id, 3, 10, con)?;
    if let Some(g) = database::get_goal(goal_id, con) {
        goal = g;
    } else {
        log::error(
            goal_id,
            &format!(
                "Could not find goal in skip_tasks({}, {}, {}, con)",
                goal_id, from, to
            ),
            con,
        );
        return Err(());
    }

    let mut end = to;
    if to >= goal.tasks.len() {
        end = goal.tasks.len();
    }
    for i in from..end {
        goal.tasks[i].status = TaskStatus::Skipped;
    }
    if let Err(_) = database::set_goal(&goal, con) {
        log::error(
            goal_id,
            &format!(
                "Could not save goal in skip_tasks({}, {}, {}, con)",
                goal_id, from, to
            ),
            con,
        );
        return Err(());
    }
    database::release_lock(goal_id, goal_id, &lock, con)?;
    // schedule next task
    return schedule_task_index(goal_id, to, 0, con);
}

pub fn abort_goal(goal_id: &str, con: &mut redis::Connection) {
    log::warning(goal_id, "Aborting Goal", con);
    if let Some(mut goal) = database::get_goal(goal_id, con) {
        goal.status = database::GoalStatus::Error;
        if let Err(_) = database::set_goal(&goal, con) {
            log::error(
                goal_id,
                &format!("Could not save goal in abort_goal({}, con)", goal_id),
                con,
            );
        }
    } else {
        log::error(
            goal_id,
            &format!("Could not find goal in abort_goal({}, con)", goal_id),
            con,
        );
    }
    log::warning(goal_id, "Goal gracefully aborted", con);
}

pub fn schedule_repeat_goal(goal_id: &str, con: &mut redis::Connection) -> Result<(), ()> {
    let lock = database::acquire_lock(goal_id, goal_id, 3, 10, con)?;
    let mut goal;
    if let Some(g) = database::get_goal(goal_id, con) {
        goal = g;
    } else {
        log::error(
            goal_id,
            &format!(
                "Could not find goal in schedule_repeat_goal({}, con)",
                goal_id
            ),
            con,
        );
        return Err(());
    }
    goal.n_repeated = goal.n_repeated + 1;
    log::info(
        goal_id,
        &format!("Repeating goal for the {} time", goal.n_repeated),
        con,
    );

    database::set_goal(&goal, con)?;
    database::release_lock(goal_id, goal_id, &lock, con)?;
    return schedule_task_index(goal_id, 0, goal.repeat_interval, con);
}

fn delete_goal(goal_id: &str) -> Result<(), ()> {
    let mut con = database::connect_redis("redis://127.0.0.1/").unwrap();
    let lock;
    if let Ok(l) = database::acquire_lock(goal_id, goal_id, 10, 10, &mut con) {
        lock = l;
    } else {
        return Err(());
    }
    let query: redis::RedisResult<i64> = con.del(&format!("goal:{}", goal_id));
    match query {
        Ok(_) => log::system(&format!("Removed goal {} from database", goal_id)),
        Err(err) => {
            log::error(
                goal_id,
                &format!("Failed to remove this goal, reason: {}", err),
                &mut con,
            );
            return Err(());
        }
    }
    if let Ok(_) = database::release_lock(goal_id, goal_id, &lock, &mut con) {
        Ok(())
    } else {
        Ok(())
    }
}

// pub fn compile_json(json: &str) -> Result<(), String> {
//     if let Ok(goal) = serde_json::from_str::<Goal>(json) {
//         // check index matching, cyclic,
//         if is_cyclic(&goal.tasks) {
//             return Err("Potential cyclic skip to found".to_string());
//         }
//         return Ok(());
//     } else {
//         return Err("JSON format Error".to_string());
//     }
// }

// pub fn is_cyclic(tasks: &Vec<Task>) -> bool {
//     let length = tasks.len();
//     if length <= 1 {
//         return false;
//     }
//     let mut tortoise = 0;
//     let mut hare = tasks[usize::try_from(tasks[0].skip_to).unwrap()].index;

//     while hare < length - 1 {
//         if tortoise == hare {
//             return true;
//         }
//         tortoise = tasks[tortoise].skip_to;
//         hare = tasks[tasks[hare].skip_to].skip_to;
//     }
//     return false;
// }
