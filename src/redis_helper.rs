extern crate redis;
pub struct Conn {
    pub connection: redis::Connection,
}
//TODO: handle ALL redis errors here
impl Conn {
    pub fn new(url: &str) -> Conn {
        let client = redis::Client::open(url).unwrap();
        let con = client.get_connection();
        Conn {
            connection: con.unwrap(),
        }
    }
    pub fn set(&mut self, key: &str, value: &str) -> redis::RedisResult<()> {
        redis::cmd("SET")
            .arg(key)
            .arg(value)
            .query(&mut self.connection)?;
        Ok(())
    }

    pub fn get(&mut self, key: &str) -> String {
        let result: String = redis::cmd("GET")
            .arg(key)
            .query(&mut self.connection)
            .unwrap();
        result
    }
    pub fn zadd(&mut self, queue: &str, item: &str, time: i64) -> i64 {
        let result = redis::cmd("ZADD")
            .arg(queue)
            .arg(time)
            .arg(item)
            .query(&mut self.connection)
            .unwrap();
        // println!("queue: {:?}", queue);
        // println!("item: {:?}", item);
        // println!("time: {:?}", time);
        result
    }
    pub fn rpush(&mut self, queue: &str, item: &str) -> i64 {
        let result = redis::cmd("RPUSH")
            .arg(queue)
            .arg(item)
            .query(&mut self.connection)
            .unwrap();
        result
    }

    pub fn blpop(&mut self, queue: &str, timeout: i64) -> Option<(String, String)> {
        let result = redis::cmd("BLPOP")
            .arg(queue)
            .arg(timeout)
            .query(&mut self.connection);
        match result {
            Ok(p) => Some(p),
            Err(_e) => None,
        }
    }
    pub fn zcheck(&mut self, queue: &str) -> Option<(String, String)> {
        let result = redis::cmd("ZRANGE")
            .arg(queue)
            .arg(0)
            .arg(0)
            .arg("WITHSCORES")
            .query(&mut self.connection);
        match result {
            Ok(p) => Some(p),
            Err(_e) => None,
        }
    }
    pub fn zrem(&mut self, queue: &str, item: &str) -> i64 {
        let result = redis::cmd("ZREM")
            .arg(queue)
            .arg(item)
            .query(&mut self.connection)
            .unwrap();
        result
    }
}
