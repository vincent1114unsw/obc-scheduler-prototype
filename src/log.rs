use chrono::Utc;
use redis::Commands;
use serde::{Deserialize, Serialize};
#[derive(Deserialize, Serialize, Clone)]
pub struct Log {
    pub goal_id: String,
    pub log_type: LogType,
    pub content: String,
    pub time: i64,
}
#[derive(Deserialize, Serialize, PartialEq, Clone)]
pub enum LogType {
    Info,
    Warning,
    Error,
}

fn log(id: &str, content: &str, log_type: LogType, con: &mut redis::Connection) {
    let out = Log {
        goal_id: id.to_string(),
        log_type,
        content: content.to_string(),
        time: Utc::now().timestamp(),
    };

    let query: std::result::Result<bool, redis::RedisError> = redis::cmd("JSON.ARRAPPEND")
        .arg(format!("goal:{}", id))
        .arg(".logs")
        .arg(serde_json::to_string(&out).unwrap())
        .query(con);
    if let Err(_) = query {
        println!("Log could not be done, database error");
    }
    print_log(&out);
}

pub fn print_log(log: &Log) {
    let typer: String;
    match &log.log_type {
        LogType::Info => typer = "Info".to_string(),
        LogType::Warning => typer = "Warning".to_string(),
        LogType::Error => typer = "Error".to_string(),
    }
    println!(
        "[{}]({}) Goal<{}>: {}",
        &typer, log.time, &log.goal_id, &log.content
    );
}
pub fn info(id: &str, content: &str, con: &mut redis::Connection) {
    log(id, content, LogType::Info, con);
}

pub fn error(id: &str, content: &str, con: &mut redis::Connection) {
    log(id, content, LogType::Error, con);
}

pub fn warning(id: &str, content: &str, con: &mut redis::Connection) {
    log(id, content, LogType::Warning, con);
}

pub fn system(content: &str) {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    let time = Utc::now().timestamp();
    let query: std::result::Result<bool, redis::RedisError> = redis::cmd("LPUSH")
        .arg("system_logs")
        .arg(format!("({}): {}", time, content))
        .query(&mut con);
    if let Err(_) = query {
        println!("Log could not be done, database error");
    }
    println!("({}): {}", time, content);
    let _: redis::RedisResult<i64> = con.publish("obc-channel", content);
}
