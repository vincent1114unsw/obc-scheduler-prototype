#![allow(dead_code)]
mod database;
mod log;
mod redis_helper;
mod scheduler;
mod tasks;
mod worker;
// use redis::Commands;
// use chrono::Utc;
// use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    println!("Hello, world!");
    // test();
    // test_locks();

    worker::start();
    scheduler::start_goal_channel().join().unwrap();
    println!("start success");

    // let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    // let mut con = client.get_connection().unwrap();
    // let x: Option<String> = con.get("x").unwrap();
    // if let Some(o) = x {
    //     println!("value: {}", o)
    // } else {
    //     println!("Not available");
    // }
}

fn numbers_loop(name: &str) {
    for i in 1..10 {
        println!("hi number {} from the {} thread!", i, name);
        thread::sleep(Duration::from_millis(500));
    }
}

// TODO: write a better test with unit testing framework
fn test() {
    // testing redisJSON
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    let task = database::Task {
        index: 0,
        priority: database::Priority::NORMAL,
        goal_id: "abcdefg".to_string(),
        skip_to: 3,
        name: "example_task".to_string(),
        args: vec![],
        next: vec![2],
        delay: 5000,
        retries: 3,
        status: database::TaskStatus::Queued,
        abort_type: database::AbortType::Panic,
        immediate_next: false,
    };
    let goal = database::Goal {
        id: "abcdefg".to_string(),
        name: "test_goal".to_string(),
        tasks: vec![task],
        n_tasks: 0,
        time: 0,
        n_repeated: 0,
        status: database::GoalStatus::Scheduled,
        repeat: 0,
        repeat_interval: 0,
        logs: vec![],
    };

    database::set_goal(&goal, &mut con).unwrap();

    let from_db = database::get_goal("abcdefg", &mut con).unwrap();

    println!("{}", serde_json::to_string(&from_db).unwrap());

    let task_db = database::get_task("abcdefg", 0, &mut con).unwrap();

    println!("{}", serde_json::to_string(&task_db).unwrap());

    let new_task = database::Task {
        index: 0,
        priority: database::Priority::NORMAL,
        goal_id: "abcdefg".to_string(),
        skip_to: 3,
        name: "example_task".to_string(),
        args: vec![],
        next: vec![2],
        delay: 5000,
        retries: 3,
        status: database::TaskStatus::Queued,
        abort_type: database::AbortType::Panic,
        immediate_next: false,
    };

    database::set_task("abcdefg", 0, &new_task, &mut con).unwrap();

    let from_db = database::get_goal("abcdefg", &mut con).unwrap();

    println!("{}", serde_json::to_string(&from_db).unwrap());

    // Testing Queue system

    let _: i64 = redis::cmd("LPUSH")
        .arg("queue:high")
        .arg("high data")
        .query(&mut con)
        .unwrap();
    let _: i64 = redis::cmd("LPUSH")
        .arg("queue:normal")
        .arg("data")
        .query(&mut con)
        .unwrap();

    let data: Vec<String> = redis::cmd("BLPOP")
        .arg("queue:urgent")
        .arg("queue:high")
        .arg("queue:normal")
        .arg(0)
        .query(&mut con)
        .unwrap();
    println!("BLPOP once data: {:?}", data);
    let data: Vec<String> = redis::cmd("BLPOP")
        .arg("queue:urgent")
        .arg("queue:high")
        .arg("queue:normal")
        .arg(0)
        .query(&mut con)
        .unwrap();
    println!("BLPOP second data: {:?}", data);
}

pub fn test_locks() {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();

    let goal = database::Goal {
        id: "lock_test_goal".to_string(),
        name: "lock_goal".to_string(),
        tasks: vec![],
        n_tasks: 0,
        time: 0,
        n_repeated: 0,
        status: database::GoalStatus::Scheduled,
        repeat: 0,
        repeat_interval: 0,
        logs: vec![],
    };

    database::set_goal(&goal, &mut con).unwrap();

    let lock = database::acquire_lock("test", "lock_test_goal", 1, 10, &mut con);
    let mut id: String;
    match lock {
        Err(_) => {
            println!("something went wrong first lock acquire");
            return;
        }
        Ok(idd) => id = idd,
    }

    if let Ok(_) = database::acquire_lock("test", "lock_test_goal", 5, 10, &mut con) {
        println!("something went wrong, shouldn't be able to acquire lock without releasing");
        return;
    }

    if let Err(_) = database::release_lock("test", "lock_test_goal", &id, &mut con) {
        println!("releasing lock failed");
        return;
    }

    let lock = database::acquire_lock("test", "lock_test_goal", 1, 10, &mut con);
    match lock {
        Err(_) => {
            println!("something went wrong second lock acquire");
            return;
        }
        Ok(idd) => id = idd,
    }
    if let Err(_) = database::release_lock("test", "lock_test_goal", &id, &mut con) {
        println!("releasing second lock failed");
        return;
    }

    println!("lock testing complete done");
}
